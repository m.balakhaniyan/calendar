// import * as $ from "jquery"
// import "./style.scss";
var PersianFormat = /** @class */ (function () {
    function PersianFormat() {
    }
    PersianFormat.isLeapYear = function (year) {
        var remain_nums = [0, 4, 8, 12, 16, 20, 24, 29, 33, 37, 41, 45, 49, 53, 57, 62, 66, 70, 74, 78, 82, 86, 90, 95, 99, 103, 107, 111, 115, 119, 124];
        return remain_nums.indexOf(year % 128) !== -1;
    };
    PersianFormat.fixNumbers = function (str, cmd) {
        if (cmd === void 0) { cmd = 0; }
        if (cmd === 0)
            for (var i = 0; i < 10; i++)
                str = str.replace(PersianFormat._persianNumbers[i], i.toString());
        else
            for (var i = 0; i < 10; i++)
                str = str.split(i.toString()).join(PersianFormat._onlyPersianNumbers[i]);
        return str;
    };
    ;
    PersianFormat.dateToGre = function (date) {
        var y = date[0], m = date[1], d = date[2];
        var gy = y + 621, gm, gd;
        gy = (m >= 10 && d >= 11) ? gy + 1 : gy;
        if (m === 11 && 1 <= d && d <= 10) {
            gy++;
        }
        else if (m == 10 && d == 11) {
            gy--;
        }
        var month_floor = [11, 10, 10, 9, 9, 9, 8, 9, 9, 10, 11, 9];
        if (d > month_floor[m]) {
            gm = (m + 3) % 12;
            gd = (d - month_floor[m]);
        }
        else {
            gm = (m + 2) % 12;
            gd = d + PersianFormat.jal_month_days[(m === 0) ? 11 : m - 1] - (month_floor[(m === 0) ? 11 : m - 1]);
        }
        gm = (gm == 0) ? 12 : gm;
        if (PersianFormat.isLeapYear(y)) {
            gd -= 1;
        }
        return new Date(gy, gm, gd);
    };
    PersianFormat.dateToTS = function (y, m, d) {
        return PersianFormat.dateToGre([y, m, d]).valueOf();
    };
    PersianFormat.tsToDate = function (date) {
        var loc = "fa-IR";
        var d = new Date(date);
        return d.toLocaleDateString(loc, {
            day: 'numeric',
            month: 'long',
            year: 'numeric'
        });
    };
    PersianFormat._jalali_months = ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند'];
    PersianFormat._persianNumbers = [/۰/g, /۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g];
    PersianFormat._onlyPersianNumbers = PersianFormat._persianNumbers.map(function (x) {
        return x.toString().substr(1, 1);
    });
    PersianFormat.jal_month_days = [31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29];
    return PersianFormat;
}());
var DateSlider = /** @class */ (function () {
    function DateSlider(option) {
        this.elem = $("#" + option['parent']).prepend("<input type=\"text\" id=\"" + option['id'] + "\" name=\"\" value=\"\" /><br>").find('#slider');
        this.start = option['from'];
        this.end = option['to'];
        this.setHeadInputs([option['start_input'], option['end_input']]);
        this.setSlider();
    }
    DateSlider.prototype.setInputsValues = function (data) {
        this.fromInput.attr('value', PersianFormat.tsToDate(data[0]));
        this.toInput.attr('value', PersianFormat.tsToDate(data[1]));
    };
    DateSlider.prototype.setSlider = function () {
        var _this = this;
        this.elem.ionRangeSlider({
            type: "double",
            grid: true,
            min: PersianFormat.dateToTS(this.start[0], this.start[1] - 1, this.start[2]),
            max: PersianFormat.dateToTS(this.end[0], this.end[1] - 1, this.end[2]),
            prettify: PersianFormat.tsToDate,
            onStart: function (data) {
                _this.setInputsValues([data.from, data.to]);
            },
            onUpdate: function (data) {
                _this.setInputsValues([data.from, data.to]);
            },
            onChange: function (data) {
                _this.setInputsValues([data.from, data.to]);
            }
        });
    };
    DateSlider.prototype.setHeadInputs = function (inputs) {
        this.fromInput = $("#" + inputs[0]);
        this.toInput = $("#" + inputs[1]);
    };
    return DateSlider;
}());
var Calendar = /** @class */ (function () {
    function Calendar(option) {
        var _this = this;
        this.state = 1;
        var _self = this;
        this.sliderHeader = option['header'];
        this.slider = option['slider'];
        this.elem = $("#" + option['id']);
        var initialDate = [PersianFormat._jalali_months[option['initial_date'][1]], PersianFormat.fixNumbers(option['initial_date'][0].toString(), 1), PersianFormat.fixNumbers(option['initial_date'][2].toString(), 1)].join(' ');
        var initialDateArray = initialDate.split(' ');
        initialDate = [initialDateArray[2], initialDateArray[0], initialDateArray[1]].join(' ');
        this.elem
            .addClass('date-input')
            .attr('value', initialDate)
            .prop('readonly', true)
            .wrap('<div class="date-input-holder"></div>');
        this.holder = this.elem.parent();
        this.addCalendarParts();
        this.elem.on('click', function () {
            _self.updateTable();
            _this.elem.parent().find('.calendar').css('visibility', 'visible');
        });
        $("html").on('click', function (e) {
            if ($(e.target).closest(_self.holder).length === 0) {
                if ((_self.holder.find('.calendar').css('visibility')) == 'visible') {
                    _self.holder.find('.calendar').css('visibility', 'hidden')
                        .find('tbody td').css('visibility', 'hidden');
                }
            }
        });
        this.fromTo = [option['from'], option['to']];
        this.createCalendar();
        this.addMonthYearData();
    }
    Calendar.prototype.setCalendar = function () {
        this.holder.append("<div class=\"calendar\"></div>");
    };
    Calendar.prototype.addCalendarParts = function () {
        this.setCalendar();
        this.holder.find('.calendar').append("<div class=\"header\">\n                <span class=\"change prev\">\u0642\u0628\u0644\u06CC</span>\n                <span class=\"select-holder\"><select class=\"year\"></select><select class=\"month\"></select></span>\n                <span class=\"change next\">\u0628\u0639\u062F\u06CC</span>\n                </div><div class=\"body\"><table><thead></thead><tbody></tbody></table></div><div class=\"footer today\">\u0627\u0645\u0631\u0648\u0632</div>");
        this.next = this.holder.find('.next');
        this.prev = this.holder.find('.prev');
        this.year = this.holder.find('.year');
        this.month = this.holder.find('.month');
        this.today = this.holder.find('.today');
        this.thead = this.holder.find('thead');
        this.tbody = this.holder.find('tbody');
    };
    Calendar.prototype.addMonthYearData = function () {
        for (var _i = 0, _a = PersianFormat._jalali_months; _i < _a.length; _i++) {
            var month = _a[_i];
            this.month.append("<option value=\"" + month + "\">" + month + "</option>");
        }
        for (var i = this.fromTo[0]; i <= this.fromTo[1]; i++) {
            this.year.append("<option value=\"" + i + "\">" + PersianFormat.fixNumbers((i).toString(), 1) + "</option>");
        }
        this.month.find('.option').css('visibility', 'visible');
        this.year.find('.option').css('visibility', 'visible');
    };
    Calendar.prototype.setTableRows = function () {
        for (var _i = 0, _a = ['', 'یک', 'دو', 'سه ', 'چهار', 'پنج', 'جمعه'].reverse(); _i < _a.length; _i++) {
            var day_name = _a[_i];
            this.thead.append("<td>" + ((day_name == 'جمعه') ? day_name : day_name + 'شنبه') + "</td>");
        }
        for (var j = 1; j <= 6; j++) {
            var tr = $("<tr></tr>");
            for (var i = 1; i <= 7; i++) {
                tr.append("<td class=\"" + ((i == 1) ? "weekend" : "") + "\"><span class=\"jalali\"></span><span class=\"gregorian\"></span></td>");
            }
            this.tbody.append(tr);
        }
    };
    Calendar.prototype.showCalendar = function () {
        var elem_date = PersianFormat.fixNumbers(this.elem.val().toString()).split(' ');
        this.month.val(elem_date[1]);
        this.year.val(elem_date[2]);
    };
    Calendar.prototype.setMonthYear = function () {
        this.updateTable();
        $(this).find('option').css('visibility', 'visible');
    };
    Calendar.prototype.updateTable = function () {
        var _this = this;
        var getGregorianDate = function (day) {
            return PersianFormat.dateToGre([this_year, mon_index, day]).getDate().toString();
        };
        var this_month = this.month.val().toString();
        var this_year = parseInt(this.year.val().toString());
        var mon_index = PersianFormat._jalali_months.indexOf(this_month);
        var g_date = PersianFormat.dateToGre([(this_year), mon_index, 1]);
        var weekday = g_date.getDay();
        var j = 0, i = 1;
        var m_days = PersianFormat.jal_month_days[PersianFormat._jalali_months.indexOf(this_month)];
        weekday = ((weekday + 1) % 7);
        var isLeap = PersianFormat.isLeapYear(this_year);
        this.tbody.find('tr').each(function () {
            var tds = $(this).find('td');
            for (var k = 0; k < 7; k++) {
                if (i <= m_days || (mon_index === 11 && isLeap && i === 30)) {
                    if (j >= weekday) {
                        $(tds[6 - k]).css('visibility', 'visible')
                            .attr('data-date', i).find('.jalali')
                            .text(PersianFormat.fixNumbers((i).toString(), 1))
                            .parent().find('.gregorian')
                            .text(getGregorianDate(i++));
                    }
                    else
                        $(tds[6 - k]).css('visibility', 'hidden');
                }
                else
                    $(tds[6 - k]).css('visibility', 'hidden');
                j++;
            }
        });
        this.holder.find('td.today').removeClass('today');
        this.holder.find('td.selected-day').removeClass('selected-day');
        this.setToday(1);
        setTimeout(function () {
            _this.selectedDay();
        }, 10);
    };
    Calendar.prototype.nextPrev = function (cmd) {
        if (cmd === void 0) { cmd = 0; }
        var _self = this;
        var month_num = (PersianFormat._jalali_months.indexOf(_self.month.val().toString()));
        var year_num = parseInt(_self.year.val().toString());
        if (cmd == 1) {
            if (month_num === 11) {
                if (year_num !== this.fromTo[1]) {
                    _self.year.val((year_num + 1).toString());
                    _self.month.val(PersianFormat._jalali_months[0]);
                }
            }
            else
                _self.month.val(PersianFormat._jalali_months[month_num + 1]);
        }
        else {
            if (month_num === 0) {
                if (year_num != this.fromTo[0]) {
                    _self.year.val((year_num - 1).toString());
                    _self.month.val(PersianFormat._jalali_months[11]);
                }
            }
            else
                _self.month.val(PersianFormat._jalali_months[month_num - 1]);
        }
        _self.holder.find('td').removeClass('today');
        _self.updateTable();
        _self.selectedDay();
    };
    Calendar.prototype.setToday = function (cmd) {
        var _this = this;
        if (cmd === void 0) { cmd = 0; }
        var today = (PersianFormat.fixNumbers(new Date().toLocaleDateString('fa-IR')).split('/'));
        if (cmd === 0) {
            this.month.val(PersianFormat._jalali_months[parseInt(today[1]) - 1]);
            this.year.val(today[0]);
            this.updateTable();
        }
        var calDate = [this.year.val().toString(), (PersianFormat._jalali_months.indexOf(this.month.val().toString()) + 1).toString()];
        setTimeout(function () {
            if (today[0] === calDate[0] && today[1] === calDate[1])
                _this.tbody.find("td[data-date=" + today[2] + "]").addClass('today');
        }, 10);
    };
    Calendar.prototype.selectedDay = function () {
        var _this = this;
        var sel_year = parseInt((this.year.val()).toString());
        var sel_month = PersianFormat._jalali_months.indexOf((this.month.val()).toString());
        var date = PersianFormat.fixNumbers(this.elem.val().toString()).split(' ');
        if (sel_month === PersianFormat._jalali_months.indexOf(date[1]) && sel_year === parseInt(date[2])) {
            setTimeout(function () {
                _this.tbody
                    .find("td[data-date=" + date[0] + "]").addClass('selected-day');
            }, 10);
        }
    };
    Calendar.prototype.updateSlider = function (day) {
        var month = PersianFormat._jalali_months.indexOf(this.month.val().toString());
        var year = parseInt(this.year.val().toString());
        if (this.sliderHeader === 'start') {
            $("#" + this.slider).data("ionRangeSlider").update({
                from: PersianFormat.dateToTS(year, month, day)
            });
        }
        else if (this.sliderHeader === 'end') {
            $("#" + this.slider).data("ionRangeSlider").update({
                to: PersianFormat.dateToTS(year, month, day)
            });
        }
        else {
            this.elem.val([(PersianFormat.fixNumbers(day.toString(), 1)), PersianFormat._jalali_months[month], PersianFormat.fixNumbers(year.toString(), 1)].join(' '));
        }
    };
    Calendar.prototype.createCalendar = function () {
        var _this = this;
        var _self = this;
        this.elem.on('click', function () {
            _this.showCalendar();
        });
        this.month.on('change', function () {
            _this.setMonthYear();
        });
        this.year.on('change', function () {
            _this.setMonthYear();
        });
        this.prev.on('click', function () {
            _this.nextPrev();
        });
        this.next.on('click', function () {
            _this.nextPrev(1);
        });
        this.today.on('click', function () {
            _self.setToday();
        });
        this.setTableRows();
        this.holder.find('td').on('click', function () {
            var day = parseInt($(this).attr('data-date'));
            _self.tbody.find('td.selected-day').removeClass('selected-day');
            _self.updateSlider(day);
            _self.selectedDay();
        });
    };
    return Calendar;
}());
var PBReport = /** @class */ (function () {
    function PBReport(option) {
        $("#" + option['main']).append('<div class="date-input-holder"><input id="start"/><br><input id="end"/></div>');
        new Calendar({
            id: 'start',
            initial_date: [1399, 3, 24],
            from: 1390,
            to: 1399,
            slider: 'slider',
            header: 'stat'
        });
        new Calendar({
            id: 'end',
            initial_date: [1399, 3, 24],
            from: 1390,
            to: 1399,
            slider: 'slider',
            header: 'ed'
        });
        // new DateSlider({
        //     parent: 'main',
        //     id: "slider",
        //     from: [1390, 1, 1],
        //     to: [1399, 12, 12],
        //     start_input: "start",
        //     end_input: "end",
        // });
    }
    return PBReport;
}());
new PBReport({
    main: "main"
});
//# sourceMappingURL=index.js.map