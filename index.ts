// import * as $ from "jquery"
// import "./style.scss";

class PersianFormat {
    public static readonly _jalali_months: string[] = ['فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند'];
    private static readonly _persianNumbers: RegExp[] = [/۰/g, /۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g];
    private static readonly _onlyPersianNumbers: string[] = PersianFormat._persianNumbers.map((x: RegExp) => {
        return x.toString().substr(1, 1)
    });
    public static readonly jal_month_days: number[] = [31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29];

    public static isLeapYear(year: number): boolean {
        let remain_nums = [0, 4, 8, 12, 16, 20, 24, 29, 33, 37, 41, 45, 49, 53, 57, 62, 66, 70, 74, 78, 82, 86, 90, 95, 99, 103, 107, 111, 115, 119, 124]
        return remain_nums.indexOf(year % 128) !== -1;
    }

    public static fixNumbers(str: string, cmd: number = 0): string {
        if (cmd === 0)
            for (let i = 0; i < 10; i++)
                str = str.replace(PersianFormat._persianNumbers[i], i.toString());
        else
            for (let i = 0; i < 10; i++)
                str = str.split(i.toString()).join(PersianFormat._onlyPersianNumbers[i]);
        return str;
    };

    public static dateToGre(date: number[]): Date {
        let [y, m, d] = date;
        let gy = y + 621, gm, gd;
        gy = (m >= 10 && d >= 11) ? gy + 1 : gy;
        if (m === 11 && 1 <= d && d <= 10) {
            gy++;
        } else if (m == 10 && d == 11) {
            gy--;
        }
        let month_floor = [11, 10, 10, 9, 9, 9, 8, 9, 9, 10, 11, 9];
        if (d > month_floor[m]) {
            gm = (m + 3) % 12;
            gd = (d - month_floor[m])
        } else {
            gm = (m + 2) % 12;
            gd = d + PersianFormat.jal_month_days[(m === 0) ? 11 : m - 1] - (month_floor[(m === 0) ? 11 : m - 1]);
        }
        gm = (gm == 0) ? 12 : gm;
        if (PersianFormat.isLeapYear(y)) {
            gd -= 1;
        }
        return new Date(gy, gm, gd);
    }

    public static dateToTS(y: number, m: number, d: number): number {
        return PersianFormat.dateToGre([y, m, d]).valueOf();
    }

    public static tsToDate(date: number): string {
        let loc = "fa-IR"
        let d = new Date(date);
        return d.toLocaleDateString(loc, {
            day: 'numeric',
            month: 'long',
            year: 'numeric'
        });
    }
}

class DateSlider {
    private readonly start: number[];
    private readonly end: number[];
    private elem: JQuery;
    private fromInput: JQuery;
    private toInput: JQuery;

    setInputsValues(data) {
        this.fromInput.attr('value', PersianFormat.tsToDate(data[0]));
        this.toInput.attr('value', PersianFormat.tsToDate(data[1]));
    }

    setSlider() {
        this.elem.ionRangeSlider({
            type: "double",
            grid: true,
            min: PersianFormat.dateToTS(this.start[0], this.start[1] - 1, this.start[2]),
            max: PersianFormat.dateToTS(this.end[0], this.end[1] - 1, this.end[2]),
            prettify: PersianFormat.tsToDate,
            onStart: (data) => {
                this.setInputsValues([data.from, data.to]);
            },
            onUpdate: (data) => {
                this.setInputsValues([data.from, data.to]);
            },
            onChange: (data) => {
                this.setInputsValues([data.from, data.to]);
            }
        });
    }

    public setHeadInputs(inputs: string[]) {
        this.fromInput = $(`#${inputs[0]}`);
        this.toInput = $(`#${inputs[1]}`);
    }

    constructor(option: object) {
        this.elem = $(`#${option['parent']}`).prepend(`<input type="text" id="${option['id']}" name="" value="" /><br>`).find('#slider');
        this.start = option['from'];
        this.end = option['to'];
        this.setHeadInputs([option['start_input'], option['end_input']])
        this.setSlider();
    }
}

class Calendar {
    private readonly elem: JQuery;
    private readonly holder: JQuery;
    private next: JQuery;
    private prev: JQuery;
    private year: JQuery;
    private month: JQuery;
    private today: JQuery;
    private thead: JQuery;
    private tbody: JQuery;
    private readonly fromTo: number[];
    private readonly sliderHeader: string;
    private readonly slider: string;

    setCalendar(): void {
        this.holder.append(`<div class="calendar"></div>`)
    }

    private addCalendarParts() {
        this.setCalendar();
        this.holder.find('.calendar').append(`<div class="header">
                <span class="change prev">قبلی</span>
                <span class="select-holder"><select class="year"></select><select class="month"></select></span>
                <span class="change next">بعدی</span>
                </div><div class="body"><table><thead></thead><tbody></tbody></table></div><div class="footer today">امروز</div>`);
        this.next = this.holder.find('.next');
        this.prev = this.holder.find('.prev');
        this.year = this.holder.find('.year');
        this.month = this.holder.find('.month');
        this.today = this.holder.find('.today');
        this.thead = this.holder.find('thead');
        this.tbody = this.holder.find('tbody');
    }

    private addMonthYearData() {
        for (let month of PersianFormat._jalali_months) {
            this.month.append(`<option value="${month}">${month}</option>`)
        }
        for (let i = this.fromTo[0]; i <= this.fromTo[1]; i++) {
            this.year.append(`<option value="${i}">${PersianFormat.fixNumbers((i).toString(), 1)}</option>`)
        }
        this.month.find('.option').css('visibility', 'visible')
        this.year.find('.option').css('visibility', 'visible')
    }

    private setTableRows() {
        for (let day_name of ['', 'یک', 'دو', 'سه ', 'چهار', 'پنج', 'جمعه'].reverse()) {
            this.thead.append(`<td>${(day_name == 'جمعه') ? day_name : day_name + 'شنبه'}</td>`);
        }
        for (let j = 1; j <= 6; j++) {
            let tr = $(`<tr></tr>`)
            for (let i = 1; i <= 7; i++) {
                tr.append(`<td class="${(i == 1) ? "weekend" : ""}"><span class="jalali"></span><span class="gregorian"></span></td>`);
            }
            this.tbody.append(tr);
        }
    }

    showCalendar() {
        let elem_date = PersianFormat.fixNumbers(this.elem.val().toString()).split(' ');
        this.month.val(elem_date[1]);
        this.year.val(elem_date[2]);
    }

    setMonthYear() {
        this.updateTable();
        $(this).find('option').css('visibility', 'visible')
    }

    updateTable() {
        let getGregorianDate = (day) => {
            return PersianFormat.dateToGre([this_year, mon_index, day]).getDate().toString()
        }
        let this_month = this.month.val().toString();
        let this_year = parseInt(this.year.val().toString());
        let mon_index = PersianFormat._jalali_months.indexOf(this_month);
        let g_date = PersianFormat.dateToGre([(this_year), mon_index, 1]);
        let weekday = g_date.getDay();
        let j = 0, i = 1;
        let m_days = PersianFormat.jal_month_days[PersianFormat._jalali_months.indexOf(this_month)];
        weekday = ((weekday + 1) % 7);
        let isLeap = PersianFormat.isLeapYear(this_year);
        this.tbody.find('tr').each(function () {
                let tds = $(this).find('td');
                for (let k = 0; k < 7; k++) {
                    if (i <= m_days || (mon_index === 11 && isLeap && i === 30)) {
                        if (j >= weekday) {
                            $(tds[6 - k]).css('visibility', 'visible')
                                .attr('data-date', i).find('.jalali')
                                .text(PersianFormat.fixNumbers((i).toString(), 1))
                                .parent().find('.gregorian')
                                .text(getGregorianDate(i++));
                        } else $(tds[6 - k]).css('visibility', 'hidden');
                    } else $(tds[6 - k]).css('visibility', 'hidden');
                    j++;
                }
            }
        );
        this.holder.find('td.today').removeClass('today');
        this.holder.find('td.selected-day').removeClass('selected-day');
        this.setToday(1);
        setTimeout(() => {
            this.selectedDay();
        }, 10);
    }

    nextPrev(cmd = 0) {
        let _self = this;
        let month_num = (PersianFormat._jalali_months.indexOf(_self.month.val().toString()));
        let year_num = parseInt(_self.year.val().toString());
        if (cmd == 1) {
            if (month_num === 11) {
                if (year_num !== this.fromTo[1]) {
                    _self.year.val((year_num + 1).toString());
                    _self.month.val(PersianFormat._jalali_months[0]);
                }
            } else
                _self.month.val(PersianFormat._jalali_months[month_num + 1]);
        } else {
            if (month_num === 0) {
                if (year_num != this.fromTo[0]) {
                    _self.year.val((year_num - 1).toString());
                    _self.month.val(PersianFormat._jalali_months[11]);
                }
            } else
                _self.month.val(PersianFormat._jalali_months[month_num - 1]);
        }
        _self.holder.find('td').removeClass('today');
        _self.updateTable();
        _self.selectedDay();
    }

    setToday(cmd = 0) {
        let today = (PersianFormat.fixNumbers(new Date().toLocaleDateString('fa-IR')).split('/'))
        if (cmd === 0) {
            this.month.val(PersianFormat._jalali_months[parseInt(today[1]) - 1]);
            this.year.val(today[0]);
            this.updateTable();
        }
        let calDate = [this.year.val().toString(), (PersianFormat._jalali_months.indexOf(this.month.val().toString()) + 1).toString()]
        setTimeout(() => {
            if (today[0] === calDate[0] && today[1] === calDate[1])
                this.tbody.find(`td[data-date=${today[2]}]`).addClass('today');
        }, 10);
    }


    selectedDay() {
        let sel_year = parseInt((this.year.val()).toString());
        let sel_month = PersianFormat._jalali_months.indexOf((this.month.val()).toString());
        let date = PersianFormat.fixNumbers(this.elem.val().toString()).split(' ');
        if (sel_month === PersianFormat._jalali_months.indexOf(date[1]) && sel_year === parseInt(date[2])) {
            setTimeout(() => {
                this.tbody
                    .find(`td[data-date=${date[0]}]`).addClass('selected-day');
            }, 10);
        }
    }

    updateSlider(day) {
        let month = PersianFormat._jalali_months.indexOf(this.month.val().toString());
        let year = parseInt(this.year.val().toString());
        if (this.sliderHeader === 'start') {
            $(`#${this.slider}`).data("ionRangeSlider").update({
                from: PersianFormat.dateToTS(year, month, day)
            });
        } else if (this.sliderHeader === 'end') {
            $(`#${this.slider}`).data("ionRangeSlider").update({
                to: PersianFormat.dateToTS(year, month, day)
            });
        } else {
            this.elem.val([(PersianFormat.fixNumbers(day.toString(), 1)), PersianFormat._jalali_months[month], PersianFormat.fixNumbers(year.toString(), 1)].join(' '))
        }
    }

    createCalendar() {
        let _self = this;
        this.elem.on('click', () => {
            this.showCalendar()
        })
        this.month.on('change', () => {
            this.setMonthYear();
        });
        this.year.on('change', () => {
            this.setMonthYear()
        });
        this.prev.on('click', () => {
            this.nextPrev();
        });
        this.next.on('click', () => {
            this.nextPrev(1);
        });
        this.today.on('click', function () {
            _self.setToday();
        })
        this.setTableRows();
        this.holder.find('td').on('click', function () {
            let day = parseInt($(this).attr('data-date'));
            _self.tbody.find('td.selected-day').removeClass('selected-day')
            _self.updateSlider(day);
            _self.selectedDay();
        });
    }

    constructor(option: object) {
        let _self = this
        this.sliderHeader = option['header'];
        this.slider = option['slider'];
        this.elem = $(`#${option['id']}`);
        let initialDate: string = [PersianFormat._jalali_months[option['initial_date'][1]], PersianFormat.fixNumbers(option['initial_date'][0].toString(), 1), PersianFormat.fixNumbers(option['initial_date'][2].toString(), 1)].join(' ')
        let initialDateArray: string[] = initialDate.split(' ');
        initialDate = [initialDateArray[2],initialDateArray[0],initialDateArray[1]].join(' ');
        this.elem
            .addClass('date-input')
            .attr('value', initialDate)
            .prop('readonly', true)
            .wrap('<div class="date-input-holder"></div>');
        this.holder = this.elem.parent();
        this.addCalendarParts();
        this.elem.on('click', () => {
            _self.updateTable();
            this.elem.parent().find('.calendar').css('visibility', 'visible');
        })
        $("html").on('click', function (e) {
            if ($(e.target).closest(_self.holder).length === 0) {
                if ((_self.holder.find('.calendar').css('visibility')) == 'visible') {
                    _self.holder.find('.calendar').css('visibility', 'hidden')
                        .find('tbody td').css('visibility', 'hidden');
                }
            }
        });

        this.fromTo = [option['from'], option['to']]
        this.createCalendar();
        this.addMonthYearData();
    }
}

class PBReport {
    constructor(option: object) {
        $(`#${option['main']}`).append('<div class="date-input-holder"><input id="start"/><br><input id="end"/></div>');
        new Calendar({
            id: 'start',
            initial_date: [1399, 3, 24],
            from: 1390,
            to: 1399,
            slider: 'slider',
            header: 'stat'
        });
        new Calendar({
            id: 'end',
            initial_date: [1399, 3, 24],
            from: 1390,
            to: 1399,
            slider: 'slider',
            header: 'ed'
        });
        // new DateSlider({
        //     parent: 'main',
        //     id: "slider",
        //     from: [1390, 1, 1],
        //     to: [1399, 12, 12],
        //     start_input: "start",
        //     end_input: "end",
        // });
    }
}

new PBReport({
    main: "main"
});

